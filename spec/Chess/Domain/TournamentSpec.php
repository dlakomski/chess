<?php

namespace spec\Chess\Domain;

use Chess\Domain\Game;
use Chess\Domain\Participant;
use Chess\Domain\Uuid;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class TournamentSpec extends ObjectBehavior
{
    public function let(Uuid $uuid, \DateTimeImmutable $date, Participant $participant, Game $game)
    {
        $this->beConstructedWith($uuid, $date, [$participant, $participant], [$game, $game]);
    }

    public function it_can_be_closed()
    {
        $this->close();
        $this->isClosed()->shouldReturn(true);
        $this->getEndDate()->shouldReturnAnInstanceOf(\DateTimeImmutable::class);
    }

    public function it_has_participants()
    {
        $this->getParticipants()->shouldBeArray();
        $this->getParticipants()->shouldHaveCount(2);
        $this->getParticipants()->shouldBeArrayOfObjects(Participant::class);
    }

    public function it_has_games()
    {
        $this->getGames()->shouldBeArray();
        $this->getGames()->shouldHaveCount(2);
        $this->getGames()->shouldBeArrayOfObjects(Game::class);
    }

    public function it_has_uuid(Uuid $uuid, \DateTimeImmutable $date, Participant $participant, Game $game)
    {
        $uuid->getValue()->willReturn('uuid-string');

        $this->beConstructedWith($uuid, $date, [$participant, $participant], [$game, $game]);
        $this->getUuid()->shouldReturn('uuid-string');
    }

    public function it_has_start_date()
    {
        $this->getStartDate()->shouldReturnAnInstanceOf(\DateTimeImmutable::class);
    }

    public function getMatchers()
    {
        return [
            'beArrayOfObjects' => function ($subject, $key) {
                foreach ($subject as $item) {
                    if (!$item instanceof $key) {
                        return false;
                    }
                }

                return true;
            }
        ];
    }

}
