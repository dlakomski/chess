<?php

namespace spec\Chess\Domain;

use Chess\Domain\Exception\InvalidArgumentException;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Ramsey\Uuid\Uuid;

class UuidSpec extends ObjectBehavior
{
    public function it_should_throw_exception_when_created_from_empty_string()
    {
        $this->beConstructedWith("");
        $this->shouldThrow(InvalidArgumentException::class)->duringInstantiation();
    }

    public function it_should_raise_error_when_created_from_non_string()
    {
        $this->beConstructedWith(new \DateTime());
        $this->shouldThrow(\TypeError::class)->duringInstantiation();
    }

    public function it_has_value()
    {
        $uuid = Uuid::uuid4()->toString();

        $this->beConstructedWith($uuid);
        $this->getValue()->shouldReturn($uuid);
    }

    public function it_should_be_casted_to_string()
    {
        $uuid = Uuid::uuid4()->toString();

        $this->beConstructedWith($uuid);
        $this->__toString()->shouldReturn($uuid);
    }

    public function it_can_be_compared_against_other_uuid()
    {
        $value = Uuid::uuid4()->toString();

        $this->beConstructedWith($value);
        $this->isEqual(new \Chess\Domain\Uuid($value))->shouldReturn(true);
    }
}
