<?php

namespace spec\Chess\Domain\Participant;

use Chess\Domain\Exception\InvalidArgumentException;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class NameSpec extends ObjectBehavior
{
    const TEST_NAME = 'David';

    public function let()
    {
        $this->beConstructedWith(self::TEST_NAME);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('Chess\Domain\Participant\Name');
    }

    public function it_should_throw_exception_when_created_from_empty_string()
    {
        $this->beConstructedWith("");
        $this->shouldThrow(InvalidArgumentException::class)->duringInstantiation();
    }

    public function it_should_raise_error_when_created_from_non_string()
    {
        $this->beConstructedWith(new \DateTime());
        $this->shouldThrow(\TypeError::class)->duringInstantiation();
    }

    public function it_has_value()
    {
        $this->getValue()->shouldReturn(self::TEST_NAME);
    }

    public function it_should_be_casted_to_string()
    {
        $this->__toString()->shouldReturn(self::TEST_NAME);
    }
}
