<?php

namespace spec\Chess\Domain;

use Chess\Domain\Game;
use Chess\Domain\Participant;
use Chess\Domain\Uuid;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class GameSpec extends ObjectBehavior
{
    public function let(Uuid $uuid, Participant $participant)
    {
        $uuid->getValue()->willReturn('test');
        $this->beConstructedWith($uuid, $participant, $participant);
    }

    public function it_should_have_black_player()
    {
        $this->getBlackPlayer()->shouldReturnAnInstanceOf(Participant::class);
    }

    public function it_should_have_white_player()
    {
        $this->getWhitePlayer()->shouldReturnAnInstanceOf(Participant::class);
    }
    
    public function it_allow_to_switch_players()
    {
        $this->switchPlayers()->shouldReturnAnInstanceOf(Game::class);
    }
}
