<?php

namespace spec\Chess\Domain\Calculator;

use Chess\Domain\Exception\InvalidArgumentException;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class VariationCalculatorSpec extends ObjectBehavior
{
    public function it_should_return_integer()
    {
        $this->calculate(1, 1)->shouldBeInteger();
    }

    public function it_should_return_correct_values()
    {
        $this->calculate(2, 2)->shouldReturn(2);
        $this->calculate(4, 2)->shouldReturn(12);
        $this->calculate(4, 3)->shouldReturn(24);
        $this->calculate(3, 1)->shouldReturn(3);
    }

    public function it_should_throw_exception_if_series_size_is_bigger_than_number_of_elements()
    {
        $this->shouldThrow(InvalidArgumentException::class)->during('calculate', [2, 3]);
    }
}
