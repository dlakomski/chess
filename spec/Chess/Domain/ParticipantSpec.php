<?php

namespace spec\Chess\Domain;

use Chess\Domain\Participant\Name;
use Chess\Domain\Uuid;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class ParticipantSpec extends ObjectBehavior
{
    public function let(Uuid $uuid, Name $name)
    {
        $this->beConstructedWith($uuid, $name);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('Chess\Domain\Participant');
    }

    public function it_has_name()
    {
        $this->getName()->shouldReturnAnInstanceOf(Name::class);
    }
}
