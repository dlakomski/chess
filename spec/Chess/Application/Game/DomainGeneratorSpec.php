<?php

namespace spec\Chess\Application\Game;

use Chess\Application\Uuid\Factory;
use Chess\Domain\Calculator\VariationCalculator;
use Chess\Domain\Exception\InvalidArgumentException;
use Chess\Domain\Participant;
use Chess\Domain\Uuid;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class DomainGeneratorSpec extends ObjectBehavior
{
    public function let(VariationCalculator $calculator, Factory $uuidFactory)
    {
        $calculator->calculate(2, 2)->willReturn(2);
        $calculator->calculate(4, 2)->willReturn(12);

        $uuidFactory->createUuid()->willReturn(new Uuid('test'));
        
        $this->beConstructedWith($calculator, $uuidFactory);
    }

    public function it_should_generate_games_for_participants(Participant $participant)
    {
        $this->generate([$participant, $participant])->shouldBeArray();
    }

    public function it_should_throw_exception_if_list_of_participants_is_empty()
    {
        $this->shouldThrow(InvalidArgumentException::class)->during('generate', [[]]);
    }
    
    public function it_should_throw_exception_when_odd_number_of_participants_given(Participant $participant)
    {
        $this->shouldThrow(InvalidArgumentException::class)->during('generate', [[$participant]]);
    }

    public function it_should_generate_exact_number_of_games(Participant $participant)
    {
        $this->generate([$participant, $participant])->shouldHaveCount(2);
        $this->generate([$participant, $participant, $participant, $participant])->shouldHaveCount(12);
    }
}
