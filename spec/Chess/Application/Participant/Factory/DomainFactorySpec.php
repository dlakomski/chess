<?php

namespace spec\Chess\Application\Participant\Factory;

use Chess\Application\Uuid\Factory;
use Chess\Domain\Participant;
use Chess\Domain\Uuid;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class DomainFactorySpec extends ObjectBehavior
{
    public function let(Factory $uuidFactory)
    {
        $uuidFactory->createUuid()->willReturn(new Uuid('abc'));
        $this->beConstructedWith($uuidFactory);
    }
    
    public function it_should_create_participants()
    {
        $this->createParticipant('test')->shouldReturnAnInstanceOf(Participant::class);
    }
}
