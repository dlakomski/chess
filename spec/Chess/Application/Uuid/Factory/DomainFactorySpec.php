<?php

namespace spec\Chess\Application\Uuid\Factory;

use Chess\Domain\Uuid;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Ramsey\Uuid\UuidFactoryInterface;

class DomainFactorySpec extends ObjectBehavior
{
    public function let(UuidFactoryInterface $uuidFactory, \Ramsey\Uuid\Uuid $uuid)
    {
        $uuidFactory->uuid4()->willReturn($uuid);
        $uuid->toString()->willReturn('test');

        $this->beConstructedWith($uuidFactory);
    }

    public function it_should_create_uuid_with_given_string()
    {
        $this->createUuid('test')->shouldReturnAnInstanceOf(Uuid::class);
    }

    public function it_should_create_uuid_without_string_given()
    {
        $this->createUuid()->shouldReturnAnInstanceOf(Uuid::class);
    }
}
