<?php

namespace spec\Chess\Application\Tournament\Factory;

use Chess\Application\Uuid\Factory;
use Chess\Domain\Game;
use Chess\Domain\Game\Generator;
use Chess\Domain\Participant;
use Chess\Domain\Tournament;
use Chess\Domain\Uuid;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class DomainFactorySpec extends ObjectBehavior
{
    public function let(Factory $uuidFactory, Generator $generator, Participant $participant, Game $game)
    {
        $uuidFactory->createUuid('test')->willReturn(new Uuid('test'));
        $generator->generate([$participant, $participant])->willReturn([$game, $game]);

        $this->beConstructedWith($uuidFactory, $generator);
    }

    public function it_should_create_instance_of_tournament(Participant $participant)
    {
        $this->createTournament('test', new \DateTime(), [$participant, $participant])
            ->shouldReturnAnInstanceOf(Tournament::class);
    }
}
