# Chess tournament generator

#### 4-layered architecture test application

This application allows to create chess tournaments with given number of participants and
generate game schedule for them.

It's test application that has been developed to learn something about 4-layered architecture.
 There can be some mistakes and/or errors included ;)

To install application run following commands:

```
composer install
bower install
```

After installation the tests can be run to make sure that everything is ok:

```
bin/phpunit
bin/phpspec run
```