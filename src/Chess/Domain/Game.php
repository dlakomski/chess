<?php

namespace Chess\Domain;

/**
 * Single chess game
 */
class Game
{
    /** @var Uuid */
    private $uuid;

    /** @var Participant */
    private $whitePlayer;

    /** @var Participant */
    private $blackPlayer;

    /**
     * Game constructor.
     * @param Uuid $uuid
     * @param Participant $whitePlayer
     * @param Participant $blackPlayer
     */
    public function __construct(Uuid $uuid, Participant $whitePlayer, Participant $blackPlayer)
    {
        $this->uuid = $uuid;
        $this->whitePlayer = $whitePlayer;
        $this->blackPlayer = $blackPlayer;
    }

    /**
     * Get black player
     *
     * @return Participant
     */
    public function getBlackPlayer() : Participant
    {
        return $this->blackPlayer;
    }

    /**
     * Get white player
     *
     * @return Participant
     */
    public function getWhitePlayer() : Participant
    {
        return $this->whitePlayer;
    }

    /**
     * Switch players
     *
     * @return Game
     */
    public function switchPlayers() : Game
    {
        return new Game(
            new Uuid($this->uuid->getValue() . '-reversed'), //ToDo: do something with that :/
            $this->getBlackPlayer(),
            $this->getWhitePlayer()
        );
    }
}