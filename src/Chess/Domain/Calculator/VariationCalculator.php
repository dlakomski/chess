<?php

namespace Chess\Domain\Calculator;

use Chess\Domain\Exception\InvalidArgumentException;

/**
 * Variation without repetition calculator
 */
class VariationCalculator
{
    /**
     * Calculate
     *
     * @param int $numberOfElements
     * @param int $seriesSize
     * @return int
     * @throws InvalidArgumentException
     */
    public function calculate(int $numberOfElements, int $seriesSize) : int
    {
        if ($seriesSize > $numberOfElements) {
            throw new InvalidArgumentException('Series size cannot be bigger than number of elements');
        }

        return $this->factorial($numberOfElements) / $this->factorial($numberOfElements - $seriesSize);
    }

    /**
     * Calculate factorial number
     *
     * @param int $n
     * @return int
     */
    private function factorial(int $n) : int
    {
        if ($n === 0) {
            return 1;
        }

        return $n * $this->factorial($n - 1);
    }
}
