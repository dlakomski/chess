<?php

namespace Chess\Domain;

use Chess\Domain\Exception\InvalidArgumentException;

/**
 * Universal unique identifier Value Object
 */
class Uuid
{
    /** @var string */
    private $value;

    /**
     * Uuid constructor.
     * @param string $value
     * @throws InvalidArgumentException
     */
    public function __construct(string $value)
    {
        if (empty($value)) {
            throw new InvalidArgumentException("Uuid cannot be created from empty string");
        }

        $this->value = $value;
    }

    /**
     * Get uuid value
     *
     * @return string
     */
    public function getValue() : string
    {
        return $this->value;
    }

    /**
     * Check if given Uuid is the same as current one
     *
     * @param Uuid $uuid
     * @return bool
     */
    public function isEqual(Uuid $uuid) : bool
    {
        return $this->value === $uuid->getValue();
    }

    /**
     * __toString magic method
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getValue();
    }

}