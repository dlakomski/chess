<?php

namespace Chess\Domain;

use Chess\Domain\Exception\InvalidArgumentException;

/**
 * Chess tournament
 */
class Tournament
{
    /** @var Uuid */
    private $uuid;

    /** @var \DateTimeImmutable */
    private $startDate;

    /** @var \DateTimeImmutable|null */
    private $endDate;

    /** @var Participant[] */
    private $participants = [];

    /** @var Game[] */
    private $games = [];

    /**
     * Tournament constructor.
     *
     * @param \Chess\Domain\Uuid $uuid
     * @param \DateTimeImmutable $startDate
     * @param array $participants
     * @param array $games
     * @throws InvalidArgumentException
     */
    public function __construct(Uuid $uuid, \DateTimeImmutable $startDate, array $participants, array $games)
    {
        $this->uuid = $uuid;
        $this->startDate = $startDate;

        $this->checkParticipantsCorrectness($participants);
        $this->participants = $participants;

        $this->checkGamesCorrectness($games);
        $this->games = $games;
    }

    /**
     * Get uuid value
     *
     * @return string
     */
    public function getUuid() : string
    {
        return $this->uuid->getValue();
    }

    /**
     * Check if other uuid is equals with ours
     *
     * @param Uuid $uuid
     * @return bool
     */
    public function match(Uuid $uuid) : bool
    {
        return $this->uuid->isEqual($uuid);
    }

    /**
     * Close tournament
     */
    public function close()
    {
        $this->endDate = new \DateTimeImmutable();
    }

    /**
     * Check if tournament is closed
     *
     * @return bool
     */
    public function isClosed() : bool
    {
        return !is_null($this->endDate);
    }

    /**
     * Get tournament end date
     *
     * @return \DateTimeImmutable|null
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Get array of tournament participants
     *
     * @return Participant[]
     */
    public function getParticipants() : array
    {
        return $this->participants;
    }

    /**
     * Get array of games
     *
     * @return Game[]
     */
    public function getGames() : array
    {
        return $this->games;
    }

    /**
     * Get tournament start date
     *
     * @return \DateTimeImmutable
     */
    public function getStartDate() : \DateTimeImmutable
    {
        return $this->startDate;
    }

    /**
     * @param array $participants
     * @throws InvalidArgumentException
     */
    public function checkParticipantsCorrectness(array $participants)
    {
        //ToDo: maybe create some ParticipantCollection?
        if (!$this->checkArrayPropriety($participants, Participant::class)) {
            throw new InvalidArgumentException('Participants can be only of Participant class');
        }

        if (count($participants) % 2 !== 0) {
            throw new InvalidArgumentException('Only even number of participants allowed');
        }
    }

    /**
     * @param array $games
     * @throws InvalidArgumentException
     */
    public function checkGamesCorrectness(array $games)
    {
        //ToDo: maybe create some GameCollection?
        if (!$this->checkArrayPropriety($games, Game::class)) {
            throw new InvalidArgumentException('Games can be only of Game class');
        }
    }

    /**
     * Check if array contains only objects of given class
     *
     * @param array $array
     * @param string $class
     * @return bool
     */
    private function checkArrayPropriety(array $array, string $class) : bool
    {
        return array_filter($array, function ($item) use ($class) {
            return $item instanceof $class;
        }) === $array;
    }
}