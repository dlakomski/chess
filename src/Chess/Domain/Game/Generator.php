<?php

namespace Chess\Domain\Game;

use Chess\Domain\Exception\InvalidArgumentException;
use Chess\Domain\Game;
use Chess\Domain\Participant;

/**
 * Chess games schedule generator
 */
interface Generator
{
    /**
     * Generate games for participants
     *
     * @param Participant[] $participants
     * @return Game[]
     * @throws InvalidArgumentException
     */
    public function generate(array $participants) : array;
}
