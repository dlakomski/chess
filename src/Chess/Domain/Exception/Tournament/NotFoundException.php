<?php

namespace Chess\Domain\Exception\Tournament;

use Chess\Domain\Exception\Exception;

/**
 * Exception thrown when tournament is not found in repository
 */
class NotFoundException extends Exception
{

}