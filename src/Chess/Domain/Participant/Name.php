<?php

namespace Chess\Domain\Participant;

use Chess\Domain\Exception\InvalidArgumentException;

/**
 * Participant name Value Object
 */
class Name
{
    private $value;

    /**
     * Name constructor.
     * @param string $value
     * @throws InvalidArgumentException
     */
    public function __construct(string $value)
    {
        if (empty($value)) {
            throw new InvalidArgumentException("Participant name cannot be created with empty value");
        }

        $this->value = $value;
    }

    /**
     * Get name value
     *
     * @return string
     */
    public function getValue() : string
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getValue();
    }
}