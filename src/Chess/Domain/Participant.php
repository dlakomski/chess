<?php

namespace Chess\Domain;

use Chess\Domain\Participant\Name;

/**
 * Tournament participant
 */
class Participant
{
    /** @var Uuid */
    private $uuid;

    /** @var Name */
    private $name;

    /**
     * Participant constructor.
     *
     * @param Uuid $uuid
     * @param Name $name
     */
    public function __construct(Uuid $uuid, Name $name)
    {
        $this->uuid = $uuid;
        $this->name = $name;
    }

    /**
     * Get participant name
     *
     * @return Name
     */
    public function getName()
    {
        return $this->name;
    }

}