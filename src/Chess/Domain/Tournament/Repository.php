<?php

namespace Chess\Domain\Tournament;

use Chess\Domain\Exception\Tournament\NotFoundException;
use Chess\Domain\Tournament;
use Chess\Domain\Uuid;

/**
 * Tournament repository interface
 */
interface Repository
{
    /**
     * Find all ongoing tournaments
     *
     * @return Tournament[]
     */
    public function findAllOngoing() : array;

    /**
     * Find tournament by uuid
     *
     * @param Uuid $id
     * @return Tournament
     * @throws NotFoundException
     */
    public function findOneByUuid(Uuid $id) : Tournament;

    /**
     * Add tournament to the storage
     *
     * @param Tournament $tournament
     * @return void
     */
    public function add(Tournament $tournament);
}