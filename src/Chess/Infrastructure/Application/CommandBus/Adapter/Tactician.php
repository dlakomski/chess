<?php

namespace Chess\Infrastructure\Application\CommandBus\Adapter;

use Chess\Application\CommandBus as BaseCommandBus;
use League\Tactician\CommandBus;

/**
 * Command bus adapter for Tactician library
 */
final class Tactician implements BaseCommandBus
{
    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * Tactician constructor.
     * @param CommandBus $commandBus
     */
    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    /**
     * @param object $command
     */
    public function handle($command)
    {
        $this->commandBus->handle($command);
    }

}