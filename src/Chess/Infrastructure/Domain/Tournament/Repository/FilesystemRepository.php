<?php

namespace Chess\Infrastructure\Domain\Tournament\Repository;

use Chess\Domain\Exception\Tournament\NotFoundException;
use Chess\Domain\Tournament;
use Chess\Domain\Tournament\Repository;
use Chess\Domain\Uuid;
use League\Flysystem\Filesystem;

/**
 * Tournament repository implementation using filesystem as as storage
 */
final class FilesystemRepository implements Repository
{
    /** @var Filesystem */
    private $filesystem;

    /**
     * FilesystemRepository constructor.
     * @param Filesystem $filesystem
     */
    public function __construct(Filesystem $filesystem)
    {
        $this->filesystem = $filesystem;
    }

    /**
     * Find all ongoing tournaments
     *
     * @return Tournament[]
     */
    public function findAllOngoing() : array
    {
        $tournaments = [];
        foreach ($this->filesystem->listContents() as $fileInfo) {
            $tournaments[] = unserialize($this->filesystem->read($fileInfo['basename']));
        }

        return array_filter($tournaments, function (Tournament $tournament) {
            return !$tournament->isClosed();
        });
    }

    /**
     * Find tournament by uuid
     *
     * @param Uuid $id
     * @return Tournament
     * @throws NotFoundException
     */
    public function findOneByUuid(Uuid $id) : Tournament
    {
        $value = $id->getValue();
        if ($this->filesystem->has($value)) {
            return unserialize($this->filesystem->read($value));
        }

        throw new NotFoundException(sprintf('Tournament with uuid %s does not exists', $value));
    }

    /**
     * Add tournament to the storage
     *
     * @param Tournament $tournament
     */
    public function add(Tournament $tournament)
    {
        $this->filesystem->write($tournament->getUuid(), serialize($tournament));
    }

}