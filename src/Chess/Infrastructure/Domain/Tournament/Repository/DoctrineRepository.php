<?php

namespace Chess\Infrastructure\Domain\Tournament\Repository;

use Chess\Application\Tournament\Factory;
use Chess\Domain\Exception\Tournament\NotFoundException;
use Chess\Domain\Tournament;
use Chess\Domain\Tournament\Repository;
use Chess\Domain\Uuid;
use Doctrine\ORM\EntityRepository;

/**
 * Tournament repository implementation using DoctrineORM
 */
final class DoctrineRepository extends EntityRepository implements Repository
{
    /**
     * Find all ongoing tournaments
     *
     * @return Tournament[]
     */
    public function findAllOngoing() : array
    {
        return $this->findBy(['endDate' => null]);
    }

    /**
     * @param Uuid $id
     * @return Tournament
     * @throws NotFoundException
     */
    public function findOneByUuid(Uuid $id) : Tournament
    {
        $tournament = $this->find($id->getValue());
        if (!$tournament) {
            throw new NotFoundException(sprintf('Tournament with uuid %s not found', $id->getValue()));
        }

        return $tournament;
    }

    /**
     * Add tournament to the storage
     *
     * @param Tournament $tournament
     */
    public function add(Tournament $tournament)
    {
        $this->getEntityManager()->persist($tournament);
        $this->getEntityManager()->flush();
    }

}