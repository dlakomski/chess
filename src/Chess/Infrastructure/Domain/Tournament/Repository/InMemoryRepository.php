<?php

namespace Chess\Infrastructure\Domain\Tournament\Repository;

use Chess\Domain\Exception\Tournament\NotFoundException;
use Chess\Domain\Tournament;
use Chess\Domain\Tournament\Repository;
use Chess\Domain\Uuid;

/**
 * Tournament repository implementation that stores Tournament objects in memory.
 * For testing purposes.
 */
class InMemoryRepository implements Repository
{
    /**
     * @var Tournament[]
     */
    private $tournaments = [];

    /**
     * Find all ongoing tournaments
     *
     * @return Tournament[]
     */
    public function findAllOngoing() : array
    {
        return array_filter($this->tournaments, function (Tournament $tournament) {
            return !$tournament->isClosed();
        });
    }

    /**
     * @param Uuid $id
     * @return Tournament
     * @throws NotFoundException
     */
    public function findOneByUuid(Uuid $id) : Tournament
    {
        foreach ($this->tournaments as $tournament) {
            if ($tournament->match($id)) {
                return $tournament;
            }
        }

        throw new NotFoundException(sprintf('Tournament with uuid %s could not be found', $id));
    }

    /**
     * Add tournament to the storage
     *
     * @param Tournament $tournament
     * @return void
     */
    public function add(Tournament $tournament)
    {
        $this->tournaments[] = $tournament;
    }

}