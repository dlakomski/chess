<?php

namespace Chess\Application\Command;

/**
 * Command to add score to the game
 */
final class AddGameScoreCommand
{
    /** @var string */
    public $uuid;

    /** @var int */
    public $score;
}