<?php

namespace Chess\Application\Command;

/**
 * Command to create tournament
 */
final class CreateTournamentCommand
{
    /** @var string */
    public $uuid;

    /** @var \DateTime */
    public $startDate;

    /** @var array */
    public $participants;
}