<?php

namespace Chess\Application\Command;

/**
 * Command to close tournament
 */
final class CloseTournamentCommand
{
    /** @var string */
    public $uuid;
}