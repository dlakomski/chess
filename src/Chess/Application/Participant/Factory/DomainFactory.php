<?php

namespace Chess\Application\Participant\Factory;

use Chess\Application\Participant\Factory;
use Chess\Application\Uuid\Factory as UuidFactory;
use Chess\Domain\Participant;

/**
 * Implementation of participant factory
 */
final class DomainFactory implements Factory
{
    /** @var UuidFactory */
    private $uuidFactory;

    /**
     * DomainFactory constructor.
     * @param UuidFactory $uuidFactory
     */
    public function __construct(UuidFactory $uuidFactory)
    {
        $this->uuidFactory = $uuidFactory;
    }

    /**
     * Create participant
     *
     * @param string $name
     * @return Participant
     */
    public function createParticipant(string $name) : Participant
    {
        return new Participant($this->uuidFactory->createUuid(), new Participant\Name($name));
    }

}