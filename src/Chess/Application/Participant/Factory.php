<?php

namespace Chess\Application\Participant;

use Chess\Domain\Participant;

/**
 * Participant factory interface
 */
interface Factory
{
    /**
     * Create participant
     * 
     * @param string $name
     * @return Participant
     */
    public function createParticipant(string $name) : Participant;
}