<?php

namespace Chess\Application\Game;

use Chess\Application\Uuid\Factory;
use Chess\Domain\Calculator\VariationCalculator;
use Chess\Domain\Exception\InvalidArgumentException;
use Chess\Domain\Game;
use Chess\Domain\Game\Generator;

/**
 * Game schedule generator
 */
final class DomainGenerator implements Generator
{
    /** @var Game[] */
    private $games = [];

    /** @var VariationCalculator */
    private $variationCalculator;

    /** @var Factory */
    private $uuidFactory;

    /**
     * Generator constructor.
     * @param VariationCalculator $variationCalculator
     * @param Factory $uuidFactory
     */
    public function __construct(VariationCalculator $variationCalculator, Factory $uuidFactory)
    {
        $this->variationCalculator = $variationCalculator;
        $this->uuidFactory = $uuidFactory;
    }

    /**
     * Generate games for participants
     *
     * @param array $participants
     * @return array
     * @throws InvalidArgumentException
     */
    public function generate(array $participants) : array
    {
        if (empty($participants)) {
            throw new InvalidArgumentException('Array of participants cannot be empty');
        }

        if ($this->participantsNumberIsOdd($participants)) {
            throw new InvalidArgumentException('Only even number of participants allowed');
        }

        $this->generateRounds($participants);
        $this->generateOppositeGames();
        $this->shuffleGames();

        return $this->games;
    }

    /**
     * Check if participants number is odd
     *
     * @param array $participants
     * @return bool
     */
    private function participantsNumberIsOdd(array $participants) : bool
    {
        return count($participants) % 2 === 1;
    }

    /**
     * Generate rounds
     *
     * @param array $participants
     * @throws InvalidArgumentException
     */
    private function generateRounds(array $participants)
    {
        $variations = $this->variationCalculator->calculate(count($participants), 2);

        while (count($this->games) < ($variations / 2)) {
            $this->generateRound($participants);
            $this->shiftElements($participants);
        }
    }

    /**
     * Generate single round games
     *
     * @param array $participants
     */
    private function generateRound(array $participants)
    {
        $numberOfParticipants = count($participants);
        $halfOfThem = $numberOfParticipants / 2;

        for ($i = 0; $i < $halfOfThem; $i++) {
            $this->games[] = new Game(
                $this->uuidFactory->createUuid(),
                $participants[$i],
                $participants[$i + $halfOfThem]
            );
        }
    }

    /**
     * Shift elements to allow new round generate
     *
     * @param array $participants
     */
    private function shiftElements(array &$participants)
    {
        $firstElement = array_shift($participants);

        array_unshift($participants, array_pop($participants));
        array_unshift($participants, $firstElement);
    }

    /**
     * Generate opposite games to make all variations happen
     */
    private function generateOppositeGames()
    {
        foreach ($this->games as $game) {
            $this->games[] = $game->switchPlayers();
        }
    }

    /**
     * Shuffle games
     */
    private function shuffleGames()
    {
        shuffle($this->games);
    }
}