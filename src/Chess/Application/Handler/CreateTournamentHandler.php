<?php

namespace Chess\Application\Handler;

use Chess\Application\Command\CreateTournamentCommand;
use Chess\Application\Participant\Factory as ParticipantFactory;
use Chess\Application\Tournament\Factory as TournamentFactory;
use Chess\Domain\Participant;
use Chess\Domain\Tournament\Repository;

/**
 * Handler for CreateTournamentCommand
 */
final class CreateTournamentHandler
{
    /** @var TournamentFactory */
    private $tournamentFactory;

    /** @var ParticipantFactory */
    private $participantFactory;

    /** @var Repository */
    private $repository;

    /**
     * CreateTournamentHandler constructor.
     * @param TournamentFactory $tournamentFactory
     * @param ParticipantFactory $participantFactory
     * @param Repository $repository
     */
    public function __construct(TournamentFactory $tournamentFactory, ParticipantFactory $participantFactory, Repository $repository)
    {
        $this->tournamentFactory = $tournamentFactory;
        $this->participantFactory = $participantFactory;
        $this->repository = $repository;
    }

    /**
     * Handle command
     *
     * @param CreateTournamentCommand $command
     */
    public function handle(CreateTournamentCommand $command)
    {
        $participants = $this->createParticipantArray($command);

        $tournament = $this->tournamentFactory->createTournament($command->uuid, $command->startDate, $participants);
        $this->repository->add($tournament);
    }

    /**
     * Create participant array
     *
     * @param CreateTournamentCommand $command
     * @return Participant[]
     */
    private function createParticipantArray(CreateTournamentCommand $command) : array
    {
        $participants = array_map(function (string $name) {
            return $this->participantFactory->createParticipant($name);
        }, $command->participants);

        return $participants;
    }
}
