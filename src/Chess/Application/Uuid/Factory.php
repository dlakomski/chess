<?php

namespace Chess\Application\Uuid;

use Chess\Domain\Uuid;

/**
 * Uuid factory interface
 */
interface Factory
{
    /**
     * Create Uuid
     *
     * @param string|null $value
     * @return Uuid
     */
    public function createUuid(string $value = null) : Uuid;
}