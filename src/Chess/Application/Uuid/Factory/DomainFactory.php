<?php

namespace Chess\Application\Uuid\Factory;

use Chess\Application\Uuid\Factory;
use Chess\Domain\Uuid;
use Ramsey\Uuid\UuidFactoryInterface;

/**
 * Implementation of uuid factory
 */
class DomainFactory implements Factory
{
    /** @var UuidFactoryInterface */
    private $uuidFactory;

    /**
     * DomainFactory constructor.
     * @param UuidFactoryInterface $uuidFactory
     */
    public function __construct(UuidFactoryInterface $uuidFactory)
    {
        $this->uuidFactory = $uuidFactory;
    }

    /**
     * Create uuid
     *
     * @param string|null $value
     * @return Uuid
     */
    public function createUuid(string $value = null) : Uuid
    {
        if (is_null($value)) {
            $value = $this->uuidFactory->uuid4()->toString();
        }

        return new Uuid($value);
    }

}