<?php

namespace Chess\Application\Tournament\Factory;

use Chess\Application\Tournament\Factory;
use Chess\Application\Uuid\Factory as UuidFactory;
use Chess\Domain\Game\Generator;
use Chess\Domain\Participant;
use Chess\Domain\Tournament;

/**
 * Tournament factory implementation
 */
final class DomainFactory implements Factory
{
    /** @var UuidFactory */
    private $uuidFactory;

    /** @var Generator */
    private $gameGenerator;

    /**
     * DomainFactory constructor.
     * @param UuidFactory $uuidFactory
     * @param Generator $gameGenerator
     */
    public function __construct(UuidFactory $uuidFactory, Generator $gameGenerator)
    {
        $this->uuidFactory = $uuidFactory;
        $this->gameGenerator = $gameGenerator;
    }

    /**
     * @param string $uuid
     * @param \DateTime $startDate
     * @param Participant[] $participants
     * @return Tournament
     */
    public function createTournament(string $uuid, \DateTime $startDate, array $participants) : Tournament
    {
        return new Tournament(
            $this->uuidFactory->createUuid($uuid),
            \DateTimeImmutable::createFromMutable($startDate),
            $participants,
            $this->gameGenerator->generate($participants)
        );
    }

}