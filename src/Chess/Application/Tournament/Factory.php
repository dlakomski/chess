<?php

namespace Chess\Application\Tournament;

use Chess\Domain\Participant;
use Chess\Domain\Tournament;

/**
 * Tournament factory interface
 */
interface Factory
{
    /**
     * Create tournament
     *
     * @param string $uuid
     * @param \DateTime $startDate
     * @param Participant[] $participants
     * @return Tournament
     */
    public function createTournament(string $uuid, \DateTime $startDate, array $participants) : Tournament;
}