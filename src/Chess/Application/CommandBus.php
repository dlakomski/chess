<?php

namespace Chess\Application;

/**
 * Command bus interface
 */
interface CommandBus
{
    /**
     * Handle command
     *
     * @param object $command
     */
    public function handle($command);
}