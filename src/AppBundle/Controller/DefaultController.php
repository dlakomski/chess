<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        $tournaments = $this->get('chess.domain.tournament_repository')->findAllOngoing();

        return $this->render('default/index.html.twig', [
            'tournaments' => $tournaments
        ]);
    }
}
