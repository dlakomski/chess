<?php

namespace AppBundle\Controller;

use AppBundle\Form\Type\CreateTournamentType;
use Chess\Application\Command\CreateTournamentCommand;
use Chess\Domain\Uuid;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/tournament")
 */
class TournamentController extends Controller
{
    /**
     * @Route("/create", name="tournament.create")
     */
    public function createTournament(Request $request)
    {
        $form = $this->createForm(CreateTournamentType::class, new CreateTournamentCommand());
        $form->handleRequest($request);
        
        if ($form->isValid()) {
            $uuid = $this->get('ramsay.uuid_factory')->uuid4()->toString();
            /** @var CreateTournamentCommand $command */
            $command = $form->getData();
            $command->uuid = $uuid;

            $this->get('chess.command_bus')->handle($command);

            return $this->redirect($this->generateUrl('tournament.view', ['uuid' => $uuid]));
        }
        
        return $this->render(':tournament:create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/view/{uuid}", name="tournament.view")
     */
    public function viewTournament($uuid)
    {
        $uuid = new Uuid($uuid);
        $tournament = $this->get("chess.domain.tournament_repository")->findOneByUuid($uuid);

        return $this->render(':tournament:view.html.twig', [
            'tournament' => $tournament
        ]);
    }
}