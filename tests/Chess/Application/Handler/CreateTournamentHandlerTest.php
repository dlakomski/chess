<?php

namespace Tests\Chess\Application\Handler;

use Chess\Application\Command\CreateTournamentCommand;
use Chess\Domain\Tournament;
use Chess\Domain\Uuid;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @group integration
 * @group tournament
 */
class CreateTournamentHandlerTest extends KernelTestCase
{
    /** @var ContainerInterface */
    private $container;

    public function setUp()
    {
        self::bootKernel();

        $this->container = self::$kernel->getContainer();
    }

    public function testHandle()
    {
        $command = new CreateTournamentCommand();
        $command->startDate = new \DateTime('2016-04-17');
        $command->uuid = 'test';
        $command->participants = ['A', 'B'];

        $handler = $this->container->get('chess.handler.create_new_tournament');
        $handler->handle($command);

        $tournament = $this->container->get('chess.domain.tournament_repository')->findOneByUuid(new Uuid('test'));

        $this->assertInstanceOf(Tournament::class, $tournament);
        $this->assertCount(2, $tournament->getParticipants());
        $this->assertCount(2, $tournament->getGames());
    }
}
