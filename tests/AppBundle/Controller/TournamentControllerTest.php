<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TournamentControllerTest extends WebTestCase
{
    public function testCreateTournament()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/tournament/create');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals('Create tournament', $crawler->filter('.container h1')->text());
    }
}